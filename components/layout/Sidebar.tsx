import { useState } from "react"

const Sidebar = () => {
  const [isOpen, setIsOpen] = useState(false)
  console.log('xxx')
  console.log(isOpen)
  return (
    <div className={"flex flex-col h-screen border-r border-black shrink-0 " + (isOpen ? "w-[300px]" : "w-[61px]")}>
      <div className="flex items-center h-[60px] w-full p-2.5">
        <button onClick={() => setIsOpen(!isOpen)} className="w-10 h-10 bg-gray-400"></button>
      </div>
    </div>
  )
}

export default Sidebar