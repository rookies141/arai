import Header from "./Header"
import Sidebar from "./Sidebar"

const Layout = ({ children }: { children: React.ReactElement }) => {
  return (
    <div className="flex">
      <Sidebar />
      <div className="w-full">
        <Header />
        {children}
      </div>
    </div>
  )
}

export default Layout